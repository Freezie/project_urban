<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'HouseController@index')->name('house');


Route::get('/favourite-properties', function () {
    return view('profile.favourite-properties');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/profile', 'ProfileController@index')->name('profile');
Route::resource('/profile', 'ProfileController');

//change password routes
Route::get('change-password', 'ChangePasswordController@index');
Route::post('change-password', 'ChangePasswordController@store')->name('change.password');

//verify Email
Auth::routes(['verify' => true]);

//Update users Information
Route::get('users/{user}',  ['as' => 'profile.index', 'uses' => 'UserController@edit']);
Route::patch('users/{user}/update',  ['as' => 'profile.index', 'uses' => 'UserController@update']);

Route::get('users/{user}',  ['as' => 'users.edit', 'uses' => 'UserController@edit']);
Route::patch('users/{user}/update',  ['as' => 'users.update', 'uses' => 'UserController@update']);

//Contact us
Route::get('/contact', 'ContactController@index')->name('contact');

// This is for searching for homes in the search nav menu
Route::get('/houses', 'HouseController@search')->name('results');
Route::post('/houses', 'HouseController@search')->name('results');

//Search for homes with advance search
// Route::post('/houses', 'HouseController@search')->name('houses');

//List New Properties 
Route::get('/new_properties', 'MpyaController@index')->name('new_properties');

//Featured Properties 
Route::get('/featured_properties', 'FeaturedController@index')->name('featured_properties');

//Favourite Properties 
Route::get('/favourite_properties', 'FavouriteController@index')->name('favourite_properties');

//User Dashboard
Route::get('/user-dashboard', 'DashboardController@index')->name('dashboard');

//Faq Dashboard
Route::get('/faq', 'FaqController@index')->name('faq');

//Regulations
Route::get('/regulations', 'RegulationController@index')->name('regulations');

//Show Services
Route::get('/services', 'HouseController@showServices')->name('services');

