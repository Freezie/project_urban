<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    protected $fillable = [
        'first_name','second_name', 'phone_number', 'id_number'
    ];
}
