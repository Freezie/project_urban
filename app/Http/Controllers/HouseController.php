<?php

namespace App\Http\Controllers;

use App\House;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

class HouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $properties = House::all();
        return view ('home.index', compact('properties'));        
    }

    public function search(Request $request){
        // 
        //
        $property_type = $request->get('p_types');
        $bedrooms = $request->get('bedrooms');
        $bathrooms = $request->get('bathrooms');
        $status = $request->get('status');

        $items =  DB::table('houses');

        if (!empty($property_type)) {
            $items = $items->where('listing_for', 'LIKE', "%{$property_type}%");
        }

        if (!empty($bedrooms)) {
            $items = $items->where('room', 'LIKE', "%{$bedrooms}%");
        }

        if (!empty($bathrooms)) {
            $items = $items->where('bathroom', 'LIKE', "%{$bathrooms}%");
        }
        if (!empty($status)) {
            $items = $items->where('listing_for', 'LIKE', "%{$status}%");
        }

        $items_count = $items->count();

        $properties = $items->get();
        return view('houses.index', compact('properties', 'items_count', 'bathrooms'));   
    }

    //Shouw services Page 
    public function showServices(){
        return view ('services.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\House  $house
     * @return \Illuminate\Http\Response
     */
    public function show(House $house)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\House  $house
     * @return \Illuminate\Http\Response
     */
    public function edit(House $house)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\House  $house
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, House $house)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\House  $house
     * @return \Illuminate\Http\Response
     */
    public function destroy(House $house)
    {
        //
    }
}
