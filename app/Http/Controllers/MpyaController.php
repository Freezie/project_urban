<?php

namespace App\Http\Controllers;

use App\Mpya;
use Illuminate\Http\Request;

class MpyaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view ('new_properties.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mpya  $mpya
     * @return \Illuminate\Http\Response
     */
    public function show(Mpya $mpya)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mpya  $mpya
     * @return \Illuminate\Http\Response
     */
    public function edit(Mpya $mpya)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mpya  $mpya
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mpya $mpya)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mpya  $mpya
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mpya $mpya)
    {
        //
    }
}
