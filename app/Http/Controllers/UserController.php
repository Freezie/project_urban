<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(User $user)
    {   
        $user = Auth::user();
        return view('profile.change-password', compact('user'));
    }

    public function update(User $user)
    { 
        $this->validate(request(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required|string|unique:users|regex:/^07\d{8}$/',
            'password' => 'required|min:6|confirmed'
        ]);

        $user->name = request('first_name');
        $user->name = request('last_name');
        $user->email = request('email');
        $user->email = request('phone');
        $user->password = bcrypt(request('password'));

        $user->save();

        return back();
    }
}
