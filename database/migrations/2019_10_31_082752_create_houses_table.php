<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('listing_for');
            $table->string('author');
            $table->string('Date');
            $table->string('is_featured');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('address');
            $table->string('area');
            $table->string('room');
            $table->string('bathroom');
            $table->string('balcony');
            $table->string('lounge');
            $table->string('garage');
            $table->string('image');
            $table->string('type_icon');
            $table->mediumText('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
