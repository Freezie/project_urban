@extends('layouts.dashboard_page') @section('content') <main id="main-container">
    <div class="content">
        <h2 class="content-heading">Profile</h2>
        <!-- Page title -->
        <div class="row">
            <div class="col-md-3">
                <!-- Photo & Description card -->
                <div class="profile-side">
                    <div class="profile-photo">
                        <img src="assets/images/profiles/05.jpg" class="circle" alt="">
                        <div class="prof-name"> {{Auth::user()->first_name}} {{Auth::user()->last_name}} </div>
                        <div class="prof-title"> {{Auth::user()->email}} </div>
                    </div>
                    <div class="profile-body">
                        <!-- <div class="desc mb-2">
                            Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                        </div> -->
                        <div class="prof-misc">
                            <span class="ti-time mr-2"></span> Member Since
                            {{Auth::user()->created_at->diffForHumans()}} </div>
                    </div>
                </div>
                <!-- /End Photo & Description card -->
            </div>
            <div class="col-md-9">
                <!-- Main Profile card -->
                <div class="card">
                    <ul class="nav nav-tabs card-header" id="profile" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="userProfile-tab" data-toggle="tab" href="#userProfile">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="stayHistory-tab" data-toggle="tab" href="#stayHistory">Stay
                                History</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profMessages-tab" data-toggle="tab" href="#profMessages">Messages</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="updateProfile-tab" data-toggle="tab" href="#updateProfile">Update
                                Profile</a>
                        </li>
                    </ul>
                    <div class="card-body">
                        <div class="tab-content profile-content" id="profileContent">
                            <!-- Profile detail tab -->
                            <div class="profile-tab tab-pane fade show active" id="userProfile">
                                <div class="row mb-2">
                                    <div class="col-12">
                                        <div class="subheading"> Contact Information </div>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-sm-2 text-muted">Email</div>
                                    <div class="col-sm-10"> {{ Auth::user()->email }} </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-sm-2 text-muted">Username</div>
                                    <div class="col-sm-10"> {{ Auth::user()->last_name }} </div>
                                </div>
                                <div class="row mt-4 mb-2">
                                    <div class="col-12">
                                        <div class="subheading"> Basic Information </div>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-sm-2 text-muted">Fullname</div>
                                    <div class="col-sm-10"> {{Auth::user()->name}} </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-sm-2 text-muted">Birthday</div>
                                    <div class="col-sm-10"> May 2nd 1986 </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-sm-2 text-muted">Address</div>
                                    <div class="col-sm-10"> Menlo Park Palo Alto </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-sm-2 text-muted">Phone</div>
                                    <div class="col-sm-10"> {{Auth::user()->phone}} </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-sm-2 text-muted">Gender</div>
                                    <div class="col-sm-10"> Female </div>
                                </div>
                                <!--                                 
                                <div class="row mt-4 mb-2">
                                    <div class="col-12">
                                        <div class="subheading">
                                            About
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-sm-2 text-muted">Bio</div>
                                    <div class="col-sm-10">
                                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        <br>
                                        <br>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </div>
                                </div> -->
                            </div>
                            <!-- /End Profile detail tab -->
                            <!-- Dashboard tab -->
                            <div class="tab-pane fade" id="stayHistory">
                                <div class="row">
                                    <div class="col-12">
                                        <!-- Invoice -->
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="card-header-inside"> Stay History </div>
                                                <table id="mytable" class="table table-striped table-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th>Title</th>
                                                            <th>From</th>
                                                            <th>To</th>
                                                            <th>Period</th>
                                                            <th>Rating</th>
                                                            <th>Rent</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody> @foreach($house_history as $hhistory) <tr>
                                                            <td>{{$hhistory -> title}}</td>
                                                            <td>{{$hhistory -> Date}}</td>
                                                            <td>{{$hhistory -> title}}</td>
                                                            <td>{{$hhistory -> Date}}</td>
                                                            <td><span class="pie">{{$hhistory -> balcony}}/8</span></td>
                                                            <td>{{$hhistory ->area}}</td>
                                                        </tr> @endforeach </tbody>
                                                </table>
                                            </div><!-- .card-body -->
                                        </div><!-- .card -->
                                        <!-- /End Invoice -->
                                    </div><!-- .col -->
                                </div><!-- .row -->
                            </div>
                            <!-- /End Dashboard tab -->
                            <!-- Messages tab -->
                            <div class="tab-pane fade" id="profMessages">
                                <div class="row mb-3">
                                    <div class="col-3">
                                        <button class="btn btn-alt-primary btn-block" type="button"><i class="fa fa-edit"></i> New Compose</button>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" class="form-control form-control-lg" placeholder="Search...">
                                    </div>
                                </div>
                                <ul class="message-list">
                                    <!-- Message list -->
                                    <li>
                                        <div class="check">
                                            <input type="checkbox">
                                        </div>
                                        <div class="pic-container">
                                            <div class="pic">
                                                <img src="assets/images/profiles/02.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="sender"> Harris Kane </div>
                                        <div class="title"> Nihil anim keffiyeh helvetica, craft beer </div>
                                    </li>
                                    <li>
                                        <div class="check">
                                            <input type="checkbox">
                                        </div>
                                        <div class="pic-container">
                                            <div class="pic">
                                                <img src="assets/images/profiles/01.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="sender"> Thomas Mollor </div>
                                        <div class="title"> Brunch 3 wolf moon tempor, sunt aliqua put a bird on it
                                            squid </div>
                                    </li>
                                    <li>
                                        <div class="check">
                                            <input type="checkbox">
                                        </div>
                                        <div class="pic-container">
                                            <div class="pic">
                                                <img src="assets/images/profiles/03.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="sender"> Gigi Laonard </div>
                                        <div class="title"> Leggings occaecat craft beer farm-to-table </div>
                                    </li>
                                    <li>
                                        <div class="check">
                                            <input type="checkbox">
                                        </div>
                                        <div class="pic-container">
                                            <div class="pic">
                                                <img src="assets/images/profiles/06.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="sender"> Michael Wong </div>
                                        <div class="title"> Euweuh nu bisa ngajar kuring ka lembur, kumaha? </div>
                                    </li>
                                    <li>
                                        <div class="check">
                                            <input type="checkbox">
                                        </div>
                                        <div class="pic-container">
                                            <div class="pic">
                                                <img src="assets/images/profiles/04.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="sender"> Phil Neck </div>
                                        <div class="title"> Nganggo naon urang teu tiasa hilap kana iyeu teh </div>
                                    </li>
                                    <li>
                                        <div class="check">
                                            <input type="checkbox">
                                        </div>
                                        <div class="pic-container">
                                            <div class="pic">
                                                <img src="assets/images/profiles/05.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="sender"> Jane Doe </div>
                                        <div class="title"> Ulah ngan ukur di ogo wae ateuh </div>
                                    </li>
                                    <li>
                                        <div class="check">
                                            <input type="checkbox">
                                        </div>
                                        <div class="pic-container">
                                            <div class="pic">
                                                <img src="assets/images/profiles/07.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="sender"> Chris Wilox </div>
                                        <div class="title"> Onsta kal bureno un saena kana </div>
                                    </li>
                                    <li>
                                        <div class="check">
                                            <input type="checkbox">
                                        </div>
                                        <div class="pic-container">
                                            <div class="pic">
                                                <img src="assets/images/profiles/08.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="sender"> Martin Plermo </div>
                                        <div class="title"> Teu pati teuing ari geuring tapi masih keneh </div>
                                    </li>
                                    <li>
                                        <div class="check">
                                            <input type="checkbox">
                                        </div>
                                        <div class="pic-container">
                                            <div class="pic">
                                                <img src="assets/images/profiles/09.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="sender"> Oscar Dunn </div>
                                        <div class="title"> Sing sehat badan sareng sakaluargi kasep </div>
                                    </li>
                                    <li>
                                        <div class="check">
                                            <input type="checkbox">
                                        </div>
                                        <div class="pic-container">
                                            <div class="pic">
                                                <img src="assets/images/profiles/10.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="sender"> Frans Likuwa </div>
                                        <div class="title"> Mun hoyong terang mah diajar atuh geura </div>
                                    </li>
                                </ul>
                                <!-- End Message list -->
                            </div>
                            <!-- /End Messages tab -->
                            <!-- Social tab -->
                            <div class="tab-pane fade" id="updateProfile">
                                <div class="row">
                                    <div class="col-md-12 mr-auto ml-auto">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="card-header-inside"> Update Password </div>
                                                <form method="POST" action="{{ route('change.password') }}">
                                                    @csrf

                                                    @foreach ($errors->all() as $error)
                                                    <p class="text-danger">{{ $error }}</p>
                                                    @endforeach

                                                    <div class="form-group row">
                                                        <div class="col-md-6">
                                                            <input id="password" placeholder="Current Password" type="password" class="form-control" name="current_password" autocomplete="current-password">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-md-6">
                                                            <input id="new_password" placeholder="New Password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-md-6">
                                                            <input id="new_confirm_password" placeholder="New Confirm Password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-0">
                                                        <div class="col-md-8 offset-md-4">
                                                            <button type="submit" class="btn btn-primary">
                                                                Update Password
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <hr />
                                                <div class="card-header-inside"> Update Profile Information </div>
                                                <hr />
                                                <form method="POST" action="{{ route('register') }}"> @csrf <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <input placeholder="First Name" id="first_name" value="{{ Auth::user()->first_name}}" type="text" class="form-control @error('name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="name" autofocus>
                                                                    @error('name') <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <input placeholder="Last Name" value="{{ Auth::user()->last_name}}" id="last_name" type="text" class="form-control @error('name') is-invalid @enderror" name="last_name" value="{{ old('second_name') }}" required autocomplete="name" autofocus>

                                                                </div>
                                                            </div>



                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <input id="email" value="{{ Auth::user()->email}}" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email"> @error('email') <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span> @enderror
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <input id="phone" minlength="10" value="{{ Auth::user()->phone}}" maxlength="10" type="phone" placeholder="Phone" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone"> @error('phone') <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span> @enderror
                                                                </div>
                                                            </div>


                                                            <!-- <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password"> @error('password')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span> @enderror
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

                                                                </div>
                                                            </div> -->




                                                            <div class="form-group ">
                                                                <button type="submit" class="col-md-3 btn btn-color btn-md btn-block btn-primary">Update Profile</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /End Social tab -->
                        </div>
                        <!-- .profile-content -->
                    </div>
                    <!-- .card-body -->
                </div>
                <!-- .card -->
                <!-- /End Main Profile card -->
            </div>
            <!-- .col -->
        </div>
        <!-- .row -->
    </div>
    <!-- .content -->
</main> @stop