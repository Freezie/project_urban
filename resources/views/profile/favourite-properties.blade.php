<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from storage.googleapis.com/themevessel-products/xero/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Oct 2019 09:18:13 GMT -->
<head>
    @include('shared.links')

</head>
<body id="top">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDTWJ3Z"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="page_loader"></div>

<!-- Top header start -->
@include('shared.top-header')
<!-- Top header end -->

<!-- main header start -->
@include('shared.nav')
<!-- main header end -->


<div class="user-page content-area-14">
    <div class="container">
        <div class="row">
            @include('shared.profile-nav')
            <div class="col-lg-8 col-md-12 col-sm-12">
                <div class="my-properties">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Property</th>
                            <th></th>
                            <th class="hdn">Date Added</th>
                            <th class="hdn">Views</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="image">
                                <a href="properties-details.html"><img alt="my-properties-3" src="assets/img/my-properties-3.jpg" class="img-fluid"></a>
                            </td>
                            <td>
                                <div class="inner">
                                    <a href="properties-details.html"><h2>Modern Family Home</h2></a>
                                    <figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i> 123 Kathal St. Tampa City,</figure>
                                    <div class="tag price">$ 27,000</div>
                                </div>
                            </td>
                            <td class="hdn">14.02.2018</td>
                            <td class="hdn">421</td>
                            <td class="actions">
                                <a href="#" class="edit"><i class="fa fa-pencil"></i>Edit</a>
                                <a href="#"><i class="delete fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="image">
                                <a href="properties-details.html"><img alt="my-properties" src="assets/img/my-properties.jpg"></a>
                            </td>
                            <td>
                                <div class="inner">
                                    <a href="properties-details.html"><h2>Beautiful Single Home</h2></a>
                                    <figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i> 123 Kathal St. Tampa City,</figure>
                                    <div class="tag price">$ 315,000</div>
                                </div>
                            </td>
                            <td class="hdn">4.01.2018</td>
                            <td class="hdn">266</td>
                            <td class="actions">
                                <a href="#" class="edit"><i class="fa fa-pencil"></i>Edit</a>
                                <a href="#"><i class="delete fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="image">
                                <a href="properties-details.html"><img alt="my-properties-2" src="assets/img/my-properties-2.jpg"></a>
                            </td>
                            <td>
                                <div class="inner">
                                    <a href="properties-details.html"><h2>Masons Villas</h2></a>
                                    <figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i> 123 Kathal St. Tampa City,</figure>
                                    <div class="tag price">$ 62,000</div>
                                </div>
                            </td>
                            <td class="hdn">24.03.2018</td>
                            <td class="hdn">45</td>
                            <td class="actions">
                                <a href="#" class="edit"><i class="fa fa-pencil"></i>Edit</a>
                                <a href="#"><i class="delete fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="image">
                                <a href="properties-details.html"><img alt="my-properties-3" src="assets/img/my-properties-3.jpg"></a>
                            </td>
                            <td>
                                <div class="inner">
                                    <a href="properties-details.html"><h2>Modern Family Home</h2></a>
                                    <figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i> 123 Kathal St. Tampa City,</figure>
                                    <div class="tag price">$ 27,000</div>
                                </div>
                            </td>
                            <td class="hdn">14.02.2018</td>
                            <td class="hdn">421</td>
                            <td class="actions">
                                <a href="#" class="edit"><i class="fa fa-pencil"></i>Edit</a>
                                <a href="#"><i class="delete fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="image">
                                <a href="properties-details.html"><img alt="my-properties" src="assets/img/my-properties.jpg"></a>
                            </td>
                            <td>
                                <div class="inner">
                                    <a href="properties-details.html"><h2>Beautiful Single Home</h2></a>
                                    <figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i> 123 Kathal St. Tampa City,</figure>
                                    <div class="tag price">$ 315,000</div>
                                </div>
                            </td>
                            <td class="hdn">4.01.2018</td>
                            <td class="hdn">266</td>
                            <td class="actions">
                                <a href="#" class="edit"><i class="fa fa-pencil"></i>Edit</a>
                                <a href="#"><i class="delete fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="image">
                                <a href="properties-details.html"><img alt="my-properties-2" src="assets/img/my-properties-2.jpg"></a>
                            </td>
                            <td>
                                <div class="inner">
                                    <a href="properties-details.html"><h2>Masons Villas</h2></a>
                                    <figure><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i> 123 Kathal St. Tampa City,</figure>
                                    <div class="tag price">$ 62,000</div>
                                </div>
                            </td>
                            <td class="hdn">24.03.2018</td>
                            <td class="hdn">45</td>
                            <td class="actions">
                                <a href="#" class="edit"><i class="fa fa-pencil"></i>Edit</a>
                                <a href="#"><i class="delete fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-box text-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="#"><span aria-hidden="true">«</span></a></li>
                            <li class="page-item"><a class="page-link active" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#"><span aria-hidden="true">»</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Blog end -->

<!-- intro section start -->
<!-- <div class="intro-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-12">
                <img src="assets/img/logos/logo-white.png" alt="loo">
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <div class="intro-text">
                    <h3>Looking To Sell Or Rent Your Property?</h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <a href="#" class="btn btn-md sn">Submit Now</a>
            </div>
        </div>
    </div>
</div> -->
<!-- intro section end -->

<!-- Footer start -->
@include('shared.footer')
<!-- Footer end -->

<!-- Full Page Search -->
<div id="full-page-search">
    <button type="button" class="close">×</button>
    <form action="#">
        <input type="search" value="" placeholder="type keyword(s) here" />
        <button type="button" class="btn btn-sm btn-color">Search</button>
    </form>
</div>

<!-- Property Video Modal -->
<div class="modal property-modal fade" id="propertyModal" tabindex="-1" role="dialog" aria-labelledby="propertyModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="propertyModalLabel">
                    Find Your Dream Properties
                </h5>
                <p>
                    <i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i> 123 Kathal St. Tampa City,
                </p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6 modal-left">
                        <div class="modal-left-content">
                            <div id="modalCarousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <iframe class="modalIframe" src="https://www.youtube.com/embed/5e0LxrLSzok"  allowfullscreen></iframe>
                                    </div>
                                    <div class="carousel-item">
                                        <img src="assets/img/property-1.jpg" alt="Test ALT">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="assets/img/property-1.jpg" alt="Test ALT">
                                    </div>
                                </div>
                                <a class="control control-prev" href="#modalCarousel" role="button" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="control control-next" href="#modalCarousel" role="button" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                            <div class="description"><h3>Description</h3>
                                <p>
                                    Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque
                                    massa, viverra interdum eros ut, imperdiet pellentesque mauris. Proin sit amet scelerisque
                                    risus. Donec
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 modal-right">
                        <div class="modal-right-content bg-white">
                            <strong class="price">
                                $178,000
                            </strong>
                            <section>
                                <h3>Features</h3>
                                <ul class="bullets">
                                    <li><i class="flaticon-bed"></i> Double Bed</li>
                                    <li><i class="flaticon-swimmer"></i> Swimming Pool</li>
                                    <li><i class="flaticon-bath"></i> 2 Bathroom</li>
                                    <li><i class="flaticon-car-repair"></i> Garage</li>
                                    <li><i class="flaticon-parking"></i> Parking</li>
                                    <li><i class="flaticon-theatre-masks"></i> Home Theater</li>
                                    <li><i class="flaticon-old-typical-phone"></i> Telephone</li>
                                    <li><i class="flaticon-green-park-city-space"></i> Private space</li>
                                </ul>
                            </section>
                            <section>
                                <h3>Overview</h3>
                                <dl>
                                    <dt>Area</dt>
                                    <dd>2500 Sq Ft:3400</dd>
                                    <dt>Condition</dt>
                                    <dd>New</dd>
                                    <dt>Year</dt>
                                    <dd>2018</dd>
                                    <dt>Price</dt>
                                    <dd>$178,000</dd>
                                </dl>
                            </section>
                            <a href="properties-details.html" class="btn btn-show btn-theme">Show Detail</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Off-canvas sidebar -->
<!-- <div class="off-canvas-sidebar">
    <div class="off-canvas-sidebar-wrapper">
        <div class="off-canvas-header">
            <a class="close-offcanvas" href="#"><span class="fa fa-times"></span></a>
        </div>
        <div class="off-canvas-content">
            <aside class="canvas-widget">
                <div class="logo-sitebar text-center">
                    <img src="assets/img/logos/logo.png" alt="logo">
                </div>
            </aside>
            <aside class="canvas-widget">
                <ul class="menu">
                    <li class="menu-item menu-item-has-children"><a href="index.html">Home</a></li>
                    <li class="menu-item"><a href="properties-grid-leftside.html">Properties List</a></li>
                    <li class="menu-item"><a href="properties-details.html">Property Detail</a></li>
                    <li class="menu-item"><a href="blog-single-sidebar-right.html">Blog</a></li>
                    <li class="menu-item"><a href="about-1.html">About  US</a></li>
                    <li class="menu-item"><a href="contact-1.html">Contact US</a></li>
                </ul>
            </aside>
            <aside class="canvas-widget">
                <ul class="social-icons">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-vk"></i></a></li>
                </ul>
            </aside>
        </div>
    </div>
</div> -->

@include('shared.scripts')
</body>

<!-- Mirrored from storage.googleapis.com/themevessel-products/xero/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Oct 2019 09:18:15 GMT -->
</html>