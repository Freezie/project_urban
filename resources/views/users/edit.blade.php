<form method="post" action="route('users.edit', $user)">
    {{ csrf_field() }}
    {{ method_field('patch') }}

    <input type="text" name="first_name"  value="{{ $user->first_name }}" />

    <input type="text" name="second_name"  value="{{ $user->second_name }}" />
    <input type="number" name="phone_number"  value="{{ $user->phone_number }}" />

    <input type="number" name="id_number"  value="{{ $user->id_number }}" />

    <button type="submit">Send</button>
</form>