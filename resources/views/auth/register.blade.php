@extends('layouts.auths')

@section('content')
<!-- Register page start -->
<div class="register-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 col-md-5 cnt-bg-photo d-none d-xl-block d-lg-block d-md-block" style="background-image: url(assets/img/bg-photo-2.jpg)">
                <div class="register-info">
                    <a href="index.html">
                        <img src="assets/img/logos/logo.png" alt="logo">
                    </a>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-7 align-self-center">
                <div class="content-form-box register-box">
                    <div class="login-header">
                        <h4>Create Your account</h4>
                    </div>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group">
                            <input placeholder="First Name" id="first_name" type="text" class="form-control @error('name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="name" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Last Name" id="last_name" type="text" class="form-control @error('name') is-invalid @enderror" name="last_name" value="{{ old('second_name') }}" required autocomplete="name" autofocus>
                            
                        </div>
                        <div class="form-group">
                            <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input id="phone" type="phone" minlength="10" maxlength="10" placeholder="Phone" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone">
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-color btn-md btn-block">Create New Account</button>
                        </div>
                        <div class="login-footer text-center">
                            <p>Already have an account?<a href="{{route('login')}}"> Sign In</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Register page end -->
@endsection