@extends('layouts.auths')

@section('content')
<div class="login-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-7 overview-bgi cnt-bg-photo cnt-bg-photo-2 d-none d-xl-block d-lg-block d-md-block" style="background-image: url(assets/img/bg-photo-2.jpg)">
                <div class="login-info">
                    <h3>We make spectacular</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-5 content-box p-hdn">
                <div class="content-form-box">
                    <h1 class="login-header">Login</h1>
                    <p>Please enter your user name and password to login</p>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-check checkbox-theme">
                                <input class="form-check-input" type="checkbox" value="" name="remember" id="rememberMe" {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="rememberMe">
                                    {{ __('Keep Me Signed In') }}
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-color btn-md">Login</button>
                    </form>
                </div>
                <div class="login-footer clearfix">
                    <div class="pull-left">
                        <a href="index.html"><img src="assets/img/logos/black-logo.png" alt="logo"></a>
                    </div>
                    <div class="pull-right">
                        <p>Don't have an account?<a href="register.html"> Sign Up Now</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection