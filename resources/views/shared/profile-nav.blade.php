<div class="col-lg-4 col-md-5 col-sm-12">
                <div class="user-profile-box mrb">
                        <!--header -->
                        <div class="header clearfix">
                            <h2>{{ Auth::user()->name }}</h2>
                            <h4>{{ Auth::user()->email }}</h4>
                            <img src="assets/img/avatar/avatar-2.jpg" alt="avatar" class="img-fluid profile-img">
                        </div>
                        <!-- Detail -->
                        <div class="detail clearfix">
                            <ul>
                                <li>
                                    <a href="{{url('/profile')}}" class="">
                                        <i class="flaticon-user"></i>Profile
                                    </a>
                                </li>
                                <li>
                                    <a href="my-properties.html">
                                        <i class="flaticon-house"></i>My Properties
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('favourite-properties')}}">
                                        <i class="flaticon-heart-shape-outline"></i>Favorited Properties
                                    </a>
                                </li>
                                <li>
                                    <a href="submit-property.html">
                                        <i class="flaticon-add"></i>Submit New Property
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('change-password')}}">
                                        <i class="flaticon-locked-padlock"></i>Change Password
                                    </a>
                                </li>
                                @guest
                                @else
                                <li>

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="flaticon-logout"></i>{{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                                @endguest
                                
                            </ul>
                        </div>
                    </div>
            </div>