<header class="main-header do-sticky" id="main-header-2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-expand-lg navbar-light rounded">
                    <a class="navbar-brand logo" href="index.html">
                        <img src="assets/img/logos/black-logo.png" alt="logo">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="fa fa-bars"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbar">
                        <ul class="navbar-nav ml-auto">

                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/')}}" id="home" role="button" aria-haspopup="true" aria-expanded="false">
                                    Home
                                </a>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Properties
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <!-- <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">List Layout</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="properties-list-rightside.html">Right Sidebar</a></li>
                                            <li><a class="dropdown-item" href="properties-list-leftside.html">Left Sidebar</a></li>
                                            <li><a class="dropdown-item" href="properties-list-fullwidth.html">Fullwidth</a></li>
                                        </ul>
                                    </li> -->
                                    <li><a class="dropdown-item" href="{{ url('/houses')}}">Search</a>
                                    </li>
                                    <!-- <li><a class="dropdown-item" href="{{ url('/new_properties')}}">New</a>
                                    </li> -->
                                    <li><a class="dropdown-item" href="{{ url('/featured_properties')}}">Featured</a>
                                    </li>
                                    <li><a class="dropdown-item" href="{{ url('/favourite_properties')}}">Favourite</a>
                                    </li>
                                    <!-- <li><a class="dropdown-item" href="{{ url('/favourite_properties')}}">For you</a>
                                    </li> -->
                                    <!-- <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Grid Layout</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="properties-grid-rightside.html">Right Sidebar</a></li>
                                            <li><a class="dropdown-item" href="properties-grid-leftside.html">Left Sidebar</a></li>
                                            <li><a class="dropdown-item" href="properties-grid-fullwidth.html">Fullwidth</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Map View</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="properties-map-rightside-list.html">Map List Right Sidebar</a></li>
                                            <li><a class="dropdown-item" href="properties-map-leftside-list.html">Map List Left Sidebar</a></li>
                                            <li><a class="dropdown-item" href="properties-map-rightside-grid.html">Map Grid Right Sidebar</a></li>
                                            <li><a class="dropdown-item" href="properties-map-leftside-grid.html">Map Grid Left Sidebar</a></li>
                                            <li><a class="dropdown-item" href="properties-map-full.html">Map FullWidth</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Property Detail</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="properties-details.html">Property Detail 1</a></li>
                                            <li><a class="dropdown-item" href="properties-details-2.html">Property Detail 2</a></li>
                                            <li><a class="dropdown-item" href="properties-details-3.html">Property Detail 3</a></li>
                                        </ul>
                                    </li> -->
                                </ul>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/services')}}" id="home" role="button" aria-haspopup="true" aria-expanded="false">
                                    Services
                                </a>
                            </li>

                            <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Blog
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Classic</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="blog-classic-sidebar-right.html">Right Sidebar</a></li>
                                            <li><a class="dropdown-item" href="blog-classic-sidebar-left.html">Left Sidebar</a></li>
                                            <li><a class="dropdown-item" href="blog-classic-fullwidth.html">FullWidth</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Columns</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="blog-columns-2col.html">2 Columns</a></li>
                                            <li><a class="dropdown-item" href="blog-columns-3col.html">3 Columns</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Masonry</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="blog-masonry-2col.html">2 Masonry</a></li>
                                            <li><a class="dropdown-item" href="blog-masonry-3col.html">3 Masonry</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Blog Details</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="blog-single-sidebar-right.html">Right Sidebar</a></li>
                                            <li><a class="dropdown-item" href="blog-single-sidebar-left.html">Left Sidebar</a></li>
                                            <li><a class="dropdown-item" href="blog-single-fullwidth.html">Fullwidth</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li> -->
                            <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Shop
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                    <a class="dropdown-item" href="shop-list.html">Shop List</a>
                                    <a class="dropdown-item" href="shop-cart.html">Shop Cart</a>
                                    <a class="dropdown-item" href="shop-checkout.html">Shop Checkout</a>
                                    <a class="dropdown-item" href="shop-single.html">Shop Details</a>
                                </div>
                            </li> -->





                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/contact')}}" id="contact" role="button" aria-haspopup="true" aria-expanded="false">
                                    Contact
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/faq')}}" id="profile" role="button" aria-haspopup="true" aria-expanded="false">
                                    Faq
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/regulations')}}" id="profile" role="button" aria-haspopup="true" aria-expanded="false">
                                    Housing Regulations
                                </a>
                            </li>

                            @if (Route::has('login'))
                            <!-- <div class="top-right links"> -->
                            @auth
                            <!-- <a href="{{ url('/home') }}">Home</a> -->
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/user-dashboard')}}" id="profile" role="button" aria-haspopup="true" aria-expanded="false">
                                    Profile
                                </a>
                            </li>
                            @else
                            <!-- <a href="{{ route('login') }}">Login</a> -->
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}" id="profile" role="button" aria-haspopup="true" aria-expanded="false">
                                    Login
                                </a>
                            </li>

                            @if (Route::has('register'))
                            <!-- <a href="{{ route('register') }}">Register</a> -->
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}" id="profile" role="button" aria-haspopup="true" aria-expanded="false">
                                    Register
                                </a>
                            </li>
                            @endif
                            @endauth
                            <!-- </div> -->
                            @endif



                            <li class="nav-item dropdown">
                                <a href="#full-page-search" class="nav-link">
                                    <i class="fa fa-search"></i>
                                </a>
                            </li>
                            <!-- <li class="nav-item dropdown">
                                <a class="open-offcanvas nav-link" href="#">
                                    <span></span>
                                    <span class="fa fa-bars"></span>
                                </a>
                            </li> -->
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>