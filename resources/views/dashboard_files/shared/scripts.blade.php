<script type="text/javascript" src="dashboard/assets/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="dashboard/assets/js/popper.min.js"></script>
    <script type="text/javascript" src="dashboard/assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="dashboard/assets/js/holder.js"></script>
    <script type="text/javascript" src="dashboard/assets/plugins/switchery/switchery.min.js"></script>
    <script type="text/javascript" src="dashboard/assets/plugins/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="dashboard/assets/plugins/counter-up-master/jquery.counterup.min.js"></script>
    <script type="text/javascript" src="dashboard/assets/plugins/chart.js/Chart.bundle.js"></script>
    <script src="dashboard/assets/plugins/DataTables/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('dashboard/assets/js/pages/table-data.js')}}"></script>
    <script src="dashboard/assets/plugins/peity/jquery.peity.min.js"></script>
    <script src="dashboard/assets/plugins/card-refresh/refresh.js"></script>
    <script src="dashboard/assets/js/pages/dashboard.js"></script>
    <script type="text/javascript" src="dashboard/assets/js/custom.js"></script>