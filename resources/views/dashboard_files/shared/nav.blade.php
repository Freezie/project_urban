<nav id="sidebar" class="sidenav">
    <div class="sidebar-wrapper">
        <div class="profile-sidebar">
            <div class="avatar">
                <img src="dashboard/assets/images/profiles/05.jpg" alt="">
            </div>
            <div class="profile-name">
                {{Auth::user()->name}}
                <button class="btn-prof" type="button" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-ellipsis-v"></i>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#"><span class="icon ti-user mr-3"></span>Profile</a>
                    <a class="dropdown-item" href="#"><span class="icon ti-email mr-3"></span>Inbox</a>
                    <a class="dropdown-item" href="#"><span class="icon ti-settings mr-3"></span>Settings</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#"><span class="icon ti-power-off mr-3"></span>Logout</a>
                </div>
            </div>
            <div class="profile-title">
                {{Auth::user()->email}}</div>
        </div>
        <ul class="main-menu" id="menus">
            <li class="header">Houses</li>
            <li>
                <a href="{{ url('/')}}">
                    <span class="icon ti-help"></span>Home
                </a>
            </li>
            <li>
                <a href="{{ url('houses')}}">
                    <span class="icon ti-help"></span>Search for Houses
                </a>
            </li>
            <li class="header">Account</li>
            <li>
                <a href="{{ url('user-dashboard')}}">
                    <span class="icon ti-help"></span>Dashboard
                </a>
            </li>
            <li>
                <a href="{{ url('profile')}}">
                    <span class="icon ti-help"></span>Profile
                </a>
            </li>


            <li class="header">Utilities Data</li>
            <li>
                <a class="pr-mn collapsed" data-toggle="collapse" href="#utilities" aria-expanded="true">
                    <span class="icon ti-notepad"></span>Utilities
                </a>
                <ul id="utilities" class="collapse" data-parent="#menus">
                    <li><a href="ui_alerts.html">Water</a></li>
                    <li><a href="ui_badges.html">Electricity</a></li>
                    <li><a href="ui_buttons.html">Gabage</a></li>
                </ul>
            </li>
            <li class="header">Bills Manager</li>
            <li>
                <a class="pr-mn collapsed" data-toggle="collapse" href="#uielements" aria-expanded="true">
                    <span class="icon ti-notepad"></span>Bills
                </a>
                <ul id="uielements" class="collapse" data-parent="#menus">
                    <li><a href="ui_alerts.html">Water</a></li>
                    <li><a href="ui_badges.html">Electricity</a></li>
                    <li><a href="ui_buttons.html">Gabage</a></li>
                </ul>
            </li>

            <li class="header">Services</li>
            <li>
                <a href="{{ url('profile')}}">
                    <span class="icon ti-help"></span>Services
                </a>
            </li>
            <li>
                <a href="{{ url('profile')}}">
                    <span class="icon ti-help"></span>Active Services
                </a>
            </li>
            <li>
                <a href="{{ url('profile')}}">
                    <span class="icon ti-help"></span>Terminated Services
                </a>
            </li>
            <li class="header">Manager</li>
            @guest
            @else
            <li>
                <!-- <a href="{{ url('profile')}}">
                    <span class="icon ti-help"></span>Logout
                </a> -->
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="flaticon-logout"></i>{{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
            @endguest


        </ul>
    </div>
</nav>