<!DOCTYPE html>
<html lang="zxx">
<!-- Mirrored from storage.googleapis.com/themevessel-products/xero/contact-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Oct 2019 05:05:43 GMT -->

<head>
    <!-- Google Tag Manager -->
    <!-- End Google Tag Manager -->
    <title>XERO - Real Estate HTML Template</title>
    @include('shared.links')
</head>

<body id="top">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDTWJ3Z" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- <div class="page_loader"></div> -->
    <!-- main header start -->

  
    @yield('content')
   
 
    <script src="assets/js/jquery-2.2.0.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- <script src="assets/js/jquery.selectBox.js"></script>  -->
    <!-- causing not able to submit on click -->
    <script src="assets/js/rangeslider.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/jquery.filterizr.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/backstretch.js"></script>
    <script src="assets/js/jquery.countdown.js"></script>
    <script src="assets/js/jquery.scrollUp.js"></script>
    <script src="assets/js/particles.min.js"></script>
    <script src="assets/js/typed.min.js"></script>
    <script src="assets/js/dropzone.js"></script>
    <script src="assets/js/jquery.mb.YTPlayer.js"></script>
    <script src="assets/js/leaflet.js"></script>
    <script src="assets/js/leaflet-providers.js"></script>
    <script src="assets/js/leaflet.markercluster.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <!-- This is the js that holds the houses positioning on the map -->
    <script src="assets/js/maps.js"></script>
    <script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0N5pbJN10Y1oYFRd0MJ_v2g8W2QT74JE"></script>
    <script src="assets/js/ie-emulation-modes-warning.js"></script>




    <!-- Custom JS Script -->
    <script src="assets/js/app.js"></script>
    <!-- Custom JS Script -->
    <script src="assets/js/app.js"></script>
    <!-- The initial code-->
    <script>
        // var latitude = 51.541216;
        // var longitude = -0.095678;
        var latitude = -1.28333;
        var longitude = 36.81667;
        var providerName = 'Hydda.Full';
        generateMap(latitude, longitude, providerName, 'grid_layout');
    </script>

    <!-- End of the initial code-->

    <!-- <script language="javascript">
setTimeout(function(){
   window.location.reload(1);
}, 5000);
// }, 10000);
</script> -->
    <!-- This is the script for contact us -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                '../../../www.googletagmanager.com/gtm5445.html?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-PDTWJ3Z');
    </script>
    <!-- End of the scripts-->






</body>

</html>