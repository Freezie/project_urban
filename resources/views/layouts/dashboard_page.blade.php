<!DOCTYPE html>
<html lang="en-US">

<!-- Mirrored from html.alfisahr.com/prudence/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 23 Feb 2019 00:46:40 GMT -->

<head>
    <title>Prudence - Bootstrap Admin Template</title>

    @include('dashboard_files.shared.links')

</head>

<body>

    <div id="page-container">
        @include('dashboard_files.shared.nav')
        @include('dashboard_files.shared.header')
        @include('dashboard_files.shared.aside')

        <!-- MAIN CONTAINER -->
        @yield('content')
        @include('dashboard_files.shared.footer')


    </div>


    @include('dashboard_files.shared.scripts')
</body>

<!-- Mirrored from html.alfisahr.com/prudence/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 23 Feb 2019 00:47:42 GMT -->

</html>