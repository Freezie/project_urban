<!DOCTYPE html>
<html lang="zxx">
<!-- Mirrored from storage.googleapis.com/themevessel-products/xero/contact-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Oct 2019 05:05:43 GMT -->

<head>
    <!-- Google Tag Manager -->
    <!-- End Google Tag Manager -->
    <title>XERO - Real Estate HTML Template</title> @include('shared.links')
</head>

<body id="top">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDTWJ3Z" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="page_loader"></div>
    <!-- main header start --> 
    @include('shared.top-header') 
    @include('shared.nav') 
    @yield('content')
    @include('shared.footer') 
    @include('shared.fullpagesearch') 
    @include('shared.scripts')
</body>

</html>