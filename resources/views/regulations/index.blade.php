@extends('layouts.master')
@section('content')
<!-- Sub banner start -->
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>Typography</h1>
            <ul class="breadcrumbs">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active"><a href="/assets/docs/housing_regulation.pdf" download>Download housing_regulation</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub banner end -->

<!-- Typography 2 start -->
<div class="typography-2 content-area-2">
    <div class="container">
        <div class="hr-title hr-long center"><abbr>Heading style</abbr>
        </div>
        <div class="row">
            <div class="col-md-6 sd">
                <h1 class="heading">Heading One</h1>
                <p>The new beautiful typography, makes your bussiness or personal website look unique and professional. That's why we styled carefuly Polo in every detail.</p>
                <h2 class="heading">Heading Two</h2>
                <p>The new beautiful typography, makes your bussiness or personal website look unique and professional. That's why we styled carefuly Polo in every detail.</p>
                <h3 class="heading">Heading Three</h3>
                <p>The new beautiful typography, makes your bussiness or personal website look unique and professional. That's why we styled carefuly Polo in every detail.</p>
                <h4 class="heading">Heading Four</h4>
                <p>The new beautiful typography, makes your bussiness or personal website look unique and professional. That's why we styled carefuly Polo in every detail.</p>
                <h5 class="heading">Heading Five</h5>
                <p>The new beautiful typography, makes your bussiness or personal website look unique and professional. That's why we styled carefuly Polo in every detail.</p>
                <h6 class="heading">Heading Six</h6>
                <p>The new beautiful typography, makes your bussiness or personal website look unique and professional. That's why we styled carefuly Polo in every detail.</p>
            </div>
            <div class="col-md-6 sd">
                <h1 class="heading">Heading One</h1>
                <p>The new beautiful typography, makes your bussiness or personal website look unique and professional. That's why we styled carefuly Polo in every detail.</p>
                <h2 class="heading">Heading Two</h2>
                <p>The new beautiful typography, makes your bussiness or personal website look unique and professional. That's why we styled carefuly Polo in every detail.</p>
                <h3 class="heading">Heading Three</h3>
                <p>The new beautiful typography, makes your bussiness or personal website look unique and professional. That's why we styled carefuly Polo in every detail.</p>
                <h4 class="heading">Heading Four</h4>
                <p>The new beautiful typography, makes your bussiness or personal website look unique and professional. That's why we styled carefuly Polo in every detail.</p>
                <h5 class="heading">Heading Five</h5>
                <p>The new beautiful typography, makes your bussiness or personal website look unique and professional. That's why we styled carefuly Polo in every detail.</p>
                <h6 class="heading">Heading Six</h6>
                <p>The new beautiful typography, makes your bussiness or personal website look unique and professional. That's why we styled carefuly Polo in every detail.</p>
            </div>
        </div>

        <hr class="space">

        <div class="hr-title hr-long center"><abbr>Text alignment</abbr>
        </div>
        <div class="row">
            <div class="col-md-12 sd">
                <h3 class="text-center heading">Center aligned text.</h3>
                <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Vestibulum id metus ac nisl bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet sagittis. In tincidunt orci sit amet elementum vestibulum. Vivamus fermentum in arcu in aliquam.</p>
                <h3 class="heading">Left aligned text</h3>
                <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Vestibulum id metus ac nisl bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet sagittis. In tincidunt orci sit amet elementum vestibulum. Vivamus fermentum in arcu in aliquam.</p>
                <h3 class="text-right heading">Right aligned text</h3>
                <p class="text-right">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Vestibulum id metus ac nisl bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet sagittis. In tincidunt orci sit amet elementum vestibulum. Vivamus fermentum in arcu in aliquam.</p>
                <h3 class="text-justify heading">Justified text</h3>
                <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Vestibulum id metus ac nisl bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet sagittis. In tincidunt orci sit amet elementum vestibulum. Vivamus fermentum in arcu in aliquam.</p>
            </div>
        </div>

        <hr class="space">

        <div class="hr-title hr-long center heading"><abbr>Text Formatting</abbr></div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p><b>This is bold text</b>
                </p>
                <h3>This is big text</h3>
                <p><code>This is computer code</code>
                </p>
                <p><em>This is emphasized text</em>
                </p>
                <p><i>This is italic text</i>
                </p>
                <p>
                    <mark>This is highlighted text</mark>
                </p>
                <p><small>This is small text</small>
                </p>
                <p><strong>This is strongly emphasized text</strong>
                </p>
                <p>This is <sub>subscript</sub> and <sup>superscript</sup>
                </p>
                <p><ins>This text is inserted to the document</ins>
                </p>
                <p><del>This text is deleted from the document</del>
                </p>
            </div>
        </div>

        <hr class="space">

        <div class="hr-title hr-long center"><abbr>Text Emphasis Classes</abbr>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p class="text-muted">Muted: This text is grayed out.</p>
                <p class="text-primary">Important: Please read the instructions carefully before proceeding.</p>
                <p class="text-success">Success: Your message has been sent successfully.</p>
                <p class="text-info">Note: You must agree with the terms and conditions to complete the sign up process.</p>
                <p class="text-warning">Warning: There was a problem with your network connection.</p>
                <p class="text-danger">Error: An error has been occurred while submitting your data.</p>
            </div>
        </div>

        <hr class="space">

        <div class="hr-title hr-long center"><abbr>Text Transformation Classes</abbr>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p class="text-lowercase">The quick brown fox jumps over the lazy dog.</p>
                <p class="text-uppercase">The quick brown fox jumps over the lazy dog.</p>
                <p class="text-capitalize">The quick brown fox jumps over the lazy dog.</p>
            </div>
        </div>


        <div class="hr-title hr-long center"><abbr>Addresses</abbr></div>
        <div class="row">
            <div class="col-md-12 text-center">
                <address>
                    <strong>Twitter, Inc.</strong><br>
                    795 Folsom Ave, Suite 600<br>
                    San Francisco, CA 94107<br>
                    <abbr title="Phone">P:</abbr> (123) 456-7890
                </address>
                <address>
                    <strong>Full Name</strong><br>
                    <a href="mailto:#">first.last@example.com</a>
                </address>
            </div>
        </div>
    </div>
</div>
<!-- Typography 2 end -->
@stop