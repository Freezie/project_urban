<!DOCTYPE html>
<html lang="zxx">
<!-- Mirrored from storage.googleapis.com/themevessel-products/xero/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Oct 2019 09:18:13 GMT -->

<head> @include('shared.links') </head>

<body id="top">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDTWJ3Z" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="page_loader"></div>
    <!-- Top header start --> @include('shared.top-header')
    <!-- Top header end -->
    <!-- main header start --> @include('shared.nav')
    <!-- fetch in json forma-->
    <p>This is the start of the test Area</p>
    {{$properties}}
    <!-- <script>
        var properties = {
            !!json_encode($properties) !!
        };
        console.log(properties);
        document.write(5 + 677887);
    </script> -->
    <script type="text/javascript">
        var test = 4366;
        var bush = 'bush';
        var properties = {
            !!json_encode($properties - > toArray()) !!
        };
        document.write(test);
        document.write(bush);
        document.write(properties);
    </script>

    <p>This is the end of the test Area</p>
    <script>
        // var properties = {
        //     !!json_encode($properties - > toArray()) !!
        // };
        // var properties = {
        //     !!$properties - > toJson() !!
        // };
        // var properties = JSON.parse("{{ json_encode($properties) }}");
        var properties = [{
            "id": 1,
            "title": "Beautiful single home",
            "listing_for": "Sale",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": true,
            "latitude": -1.300771,
            "longitude": 36.811780,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-2.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }, {
            "id": 2,
            "title": "Modern Family Home",
            "listing_for": "Rent",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": false,
            "latitude": -1.291214,
            "longitude": 36.814409,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-1.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }, {
            "id": 3,
            "title": "Sweet Family home",
            "listing_for": "Rent",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": true,
            "latitude": -1.295419,
            "longitude": 36.813422,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-3.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }, {
            "id": 4,
            "title": "Park avenue",
            "listing_for": "Sale",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": true,
            "latitude": -1.315675,
            "longitude": 36.883746,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-4.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }, {
            "id": 5,
            "title": "Masons Villas",
            "listing_for": "Rent",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": false,
            "latitude": -1.285748,
            "longitude": 36.887189,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-5.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }, {
            "id": 6,
            "title": "Big Head House",
            "listing_for": "Sale",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": false,
            "latitude": -1.283572,
            "longitude": 36.889665,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-6.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }, {
            "id": 7,
            "title": "155/A Valley Street 7",
            "listing_for": "Rent",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": false,
            "latitude": -1.280708,
            "longitude": 36.890384,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-7.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }, {
            "id": 8,
            "title": "4552 Lynn Avenue 1",
            "listing_for": "Sale",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": false,
            "latitude": -1.283647,
            "longitude": 36.892211,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-8.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }, {
            "id": 9,
            "title": "4552 Lynn Avenue 2",
            "listing_for": "Sale",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": false,
            "latitude": -1.309696,
            "longitude": 36.892090,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-9.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }, {
            "id": 10,
            "title": "4552 Lynn Avenue 3",
            "listing_for": "Sale",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": false,
            "latitude": -1.305545,
            "longitude": 36.894762,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-10.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }, {
            "id": 11,
            "title": "4552 Lynn Avenue 4",
            "listing_for": "Sale",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": false,
            "latitude": 51.558907,
            "longitude": -0.041842,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-11.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }, {
            "id": 12,
            "title": "Sweet Family home",
            "listing_for": "Sale",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": true,
            "latitude": 51.550644,
            "longitude": -0.103493,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-12.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }, {
            "id": 13,
            "title": "Sweet Family home",
            "listing_for": "Sale",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": false,
            "latitude": 51.532112,
            "longitude": -0.051885,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-13.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }, {
            "id": 14,
            "title": "4552 Lynn Avenue 7",
            "listing_for": "Sale",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": true,
            "latitude": 51.531311,
            "longitude": -0.052314,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-14.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }, {
            "id": 15,
            "title": "Park avenue",
            "price": 2505.11,
            "listing_for": "Sale",
            'author': 'Jhon Doe',
            'date': '5 days ago',
            "is_featured": false,
            "latitude": 51.530189,
            "longitude": -0.078750,
            "address": "123 Kathal St. Tampa City, ",
            "area": 980,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "assets/img/property-5.jpg",
            "type_icon": "assets/img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem"
        }];
    </script>
    <!-- main header end -->
    <!-- properties map start -->
    <div class="map-content content-area container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div id="map"></div>
                </div>
            </div>
            <div class="col-lg-6 map-content-sidebar map-content-right">
                <div class="title-area">
                    <h2 class="pull-left">Search</h2>
                    <a class="show-more-options pull-right" data-toggle="collapse" data-target="#options-content">
                        <i class="fa fa-plus-circle"></i> Show More Options </a>
                    <div class="clearfix"></div>
                </div>
                <div class="properties-map-search">
                    <div class="properties-map-search-content search-area">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <select class="selectpicker search-fields" data-live-search="true" data-live-search-placeholder="Search value">
                                        <option>Area From</option>
                                        <option>1000</option>
                                        <option>800</option>
                                        <option>600</option>
                                        <option>400</option>
                                        <option>200</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <select class="selectpicker search-fields" data-live-search="true" data-live-search-placeholder="Search value">
                                        <option>Property Status</option>
                                        <option>For Sale</option>
                                        <option>For Rent</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <select class="selectpicker search-fields" data-live-search="true" data-live-search-placeholder="Search value">
                                        <option>Location</option>
                                        <option>United States</option>
                                        <option>United Kingdom</option>
                                        <option>United Kingdom</option>
                                        <option>American Samoa</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <select class="selectpicker search-fields" data-live-search="true" data-live-search-placeholder="Search value">
                                        <option>Property Types</option>
                                        <option>Residential</option>
                                        <option>Commercial</option>
                                        <option>Land</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="range-slider">
                                    <label>Area</label>
                                    <div data-min="0" data-max="10000" data-unit="Sq ft" data-min-name="min_area" data-max-name="max_area" class="range-slider-ui ui-slider" aria-disabled="false"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="range-slider">
                                    <label>Price</label>
                                    <div data-min="0" data-max="150000" data-unit="USD" data-min-name="min_price" data-max-name="max_price" class="range-slider-ui ui-slider" aria-disabled="false"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div id="options-content" class="collapse">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <select class="selectpicker search-fields" data-live-search="true" data-live-search-placeholder="Search value">
                                            <option>Bedrooms</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <select class="selectpicker search-fields" data-live-search="true" data-live-search-placeholder="Search value">
                                            <option>Bathroom</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <select class="selectpicker search-fields" data-live-search="true" data-live-search-placeholder="Search value">
                                            <option>Balcony</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <select class="selectpicker search-fields" data-live-search="true" data-live-search-placeholder="Search value">
                                            <option>Garage</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <label class="margin-t-10 map-featured">Features</label>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="checkbox checkbox-theme checkbox-circle">
                                        <input id="checkbox1" type="checkbox">
                                        <label for="checkbox1"> Free Parking </label>
                                    </div>
                                    <div class="checkbox checkbox-theme checkbox-circle">
                                        <input id="checkbox2" type="checkbox">
                                        <label for="checkbox2"> Air Condition </label>
                                    </div>
                                    <div class="checkbox checkbox-theme checkbox-circle">
                                        <input id="checkbox3" type="checkbox">
                                        <label for="checkbox3"> Places to seat </label>
                                    </div>
                                    <div class="checkbox checkbox-theme checkbox-circle">
                                        <input id="checkbox4" type="checkbox">
                                        <label for="checkbox4"> Swimming Pool </label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="checkbox checkbox-theme checkbox-circle">
                                        <input id="checkbox5" type="checkbox">
                                        <label for="checkbox5"> Laundry Room </label>
                                    </div>
                                    <div class="checkbox checkbox-theme checkbox-circle">
                                        <input id="checkbox6" type="checkbox">
                                        <label for="checkbox6"> Window Covering </label>
                                    </div>
                                    <div class="checkbox checkbox-theme checkbox-circle">
                                        <input id="checkbox7" type="checkbox">
                                        <label for="checkbox7"> Central Heating </label>
                                    </div>
                                    <div class="checkbox checkbox-theme checkbox-circle">
                                        <input id="checkbox8" type="checkbox">
                                        <label for="checkbox8"> Alarm </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="map-content-separater hidden-sm hidden-xs"></div>
                <div class="clearfix"></div>
                <div class="title-area hidden-sm hidden-xs">
                    <h2 class="pull-left">Properties</h2>
                    <div class="pull-right btns-area">
                        <a href="properties-list-rightside.html" class="change-view-btn active-view-btn"><i class="fa fa-th-list"></i></a>
                        <a href="properties-grid-leftside.html" class="change-view-btn"><i class="fa fa-th-large"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="fetching-properties hidden-sm hidden-xs"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- properties map start -->
    <!-- Featured properties start -->
    <div class="featured-properties content-area-2">
        <div class="container">
            <div class="main-title">
                <h1>Featured Properties</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
            <ul class="list-inline-listing filters filteriz-navigation">
                <li class="active btn filtr-button filtr" data-filter="all">All</li>
                <li data-filter="1" class="btn btn-inline filtr-button filtr">Audi</li>
                <li data-filter="2" class="btn btn-inline filtr-button filtr">Mercedes</li>
                <li data-filter="3" class="btn btn-inline filtr-button filtr">Ferrari</li>
            </ul>
            <div class="row filter-portfolio">
                <div class="">
                    <div class="col-lg-4 col-md-6 col-sm-12 filtr-item" data-category="3">
                        <div class="property-box-4">
                            <!-- Property Img -->
                            <div class="property-thumbnail">
                                <a href="properties-details.html" class="property-img">
                                    <img src="assets/img/property-1.jpg" alt="property-1" class="img-fluid">
                                </a>
                                <div class="property-overlay">
                                    <a href="properties-details.html" class="overlay-link">
                                        <i class="fa fa-link"></i>
                                    </a>
                                    <a class="overlay-link property-video" title="Test Title">
                                        <i class="fa fa-video-camera"></i>
                                    </a>
                                    <div class="property-magnify-gallery">
                                        <a href="assets/img/property-1.jpg" class="overlay-link">
                                            <i class="fa fa-expand"></i>
                                        </a>
                                        <a href="assets/img/property-6.jpg" class="hidden"></a>
                                        <a href="assets/img/property-4.jpg" class="hidden"></a>
                                    </div>
                                </div>
                                <div class="text">
                                    <div class="pull-left">
                                        <a href="properties-details.html">Masons Villas</a>
                                    </div>
                                    <div class="pull-right price"> $320.00 </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 filtr-item" data-category="3, 2, 1">
                        <div class="property-box-4">
                            <!-- Property Img -->
                            <div class="property-thumbnail">
                                <a href="properties-details.html" class="property-img">
                                    <img src="assets/img/property-5.jpg" alt="property-5" class="img-fluid">
                                </a>
                                <div class="property-overlay">
                                    <a href="properties-details.html" class="overlay-link">
                                        <i class="fa fa-link"></i>
                                    </a>
                                    <a class="overlay-link property-video" title="Test Title">
                                        <i class="fa fa-video-camera"></i>
                                    </a>
                                    <div class="property-magnify-gallery">
                                        <a href="assets/img/property-5.jpg" class="overlay-link">
                                            <i class="fa fa-expand"></i>
                                        </a>
                                        <a href="assets/img/property-6.jpg" class="hidden"></a>
                                        <a href="assets/img/property-4.jpg" class="hidden"></a>
                                    </div>
                                </div>
                                <div class="text">
                                    <div class="pull-left">
                                        <a href="properties-details.html">Modern Family Home</a>
                                    </div>
                                    <div class="pull-right price"> $320.00 </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 filtr-item" data-category="1, 2">
                        <div class="property-box-4">
                            <!-- Property Img -->
                            <div class="property-thumbnail">
                                <a href="properties-details.html" class="property-img">
                                    <img src="assets/img/property-7.jpg" alt="property-7" class="img-fluid">
                                </a>
                                <div class="property-overlay">
                                    <a href="properties-details.html" class="overlay-link">
                                        <i class="fa fa-link"></i>
                                    </a>
                                    <a class="overlay-link property-video" title="Test Title">
                                        <i class="fa fa-video-camera"></i>
                                    </a>
                                    <div class="property-magnify-gallery">
                                        <a href="assets/img/property-7.jpg" class="overlay-link">
                                            <i class="fa fa-expand"></i>
                                        </a>
                                        <a href="assets/img/property-6.jpg" class="hidden"></a>
                                        <a href="assets/img/property-4.jpg" class="hidden"></a>
                                    </div>
                                </div>
                                <div class="text">
                                    <div class="pull-left">
                                        <a href="properties-details.html">Real Luxury Villa</a>
                                    </div>
                                    <div class="pull-right price"> $320.00 </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 filtr-item" data-category="3, 2">
                        <div class="property-box-4">
                            <!-- Property Img -->
                            <div class="property-thumbnail">
                                <a href="properties-details.html" class="property-img">
                                    <img src="assets/img/property-4.jpg" alt="property-4" class="img-fluid">
                                </a>
                                <div class="property-overlay">
                                    <a href="properties-details.html" class="overlay-link">
                                        <i class="fa fa-link"></i>
                                    </a>
                                    <a class="overlay-link property-video" title="Test Title">
                                        <i class="fa fa-video-camera"></i>
                                    </a>
                                    <div class="property-magnify-gallery">
                                        <a href="assets/img/property-4.jpg" class="overlay-link">
                                            <i class="fa fa-expand"></i>
                                        </a>
                                        <a href="assets/img/property-6.jpg" class="hidden"></a>
                                        <a href="assets/img/property-4.jpg" class="hidden"></a>
                                    </div>
                                </div>
                                <div class="text">
                                    <div class="pull-left">
                                        <a href="properties-details.html">Beautiful Single Home</a>
                                    </div>
                                    <div class="pull-right price"> $320.00 </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 filtr-item" data-category="2, 1">
                        <div class="property-box-4">
                            <!-- Property Img -->
                            <div class="property-thumbnail">
                                <a href="properties-details.html" class="property-img">
                                    <img src="assets/img/property-9.jpg" alt="property-9" class="img-fluid">
                                </a>
                                <div class="property-overlay">
                                    <a href="properties-details.html" class="overlay-link">
                                        <i class="fa fa-link"></i>
                                    </a>
                                    <a class="overlay-link property-video" title="Test Title">
                                        <i class="fa fa-video-camera"></i>
                                    </a>
                                    <div class="property-magnify-gallery">
                                        <a href="assets/img/property-9.jpg" class="overlay-link">
                                            <i class="fa fa-expand"></i>
                                        </a>
                                        <a href="assets/img/property-2.jpg" class="hidden"></a>
                                        <a href="assets/img/property-4.jpg" class="hidden"></a>
                                    </div>
                                </div>
                                <div class="text">
                                    <div class="pull-left">
                                        <a href="properties-details.html">Relaxing Apartment</a>
                                    </div>
                                    <div class="pull-right price"> $320.00 </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 filtr-item" data-category="3, 1, 2">
                        <div class="property-box-4">
                            <!-- Property Img -->
                            <div class="property-thumbnail">
                                <a href="properties-details.html" class="property-img">
                                    <img src="assets/img/property-6.jpg" alt="property-6" class="img-fluid">
                                </a>
                                <div class="property-overlay">
                                    <a href="properties-details.html" class="overlay-link">
                                        <i class="fa fa-link"></i>
                                    </a>
                                    <a class="overlay-link property-video" title="Test Title">
                                        <i class="fa fa-video-camera"></i>
                                    </a>
                                    <div class="property-magnify-gallery">
                                        <a href="assets/img/property-6.jpg" class="overlay-link">
                                            <i class="fa fa-expand"></i>
                                        </a>
                                        <a href="assets/img/property-1.jpg" class="hidden"></a>
                                        <a href="assets/img/property-4.jpg" class="hidden"></a>
                                    </div>
                                </div>
                                <div class="text">
                                    <div class="pull-left">
                                        <a href="properties-details.html">Sweet Family Home</a>
                                    </div>
                                    <div class="pull-right price"> $320.00 </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Featured properties end -->
    <!-- services start -->
    <!-- services end -->
    <!-- Pricing tables start -->
    <!-- Pricing tables end -->
    <!-- Agent start -->
    <!-- Agent end -->
    <!-- Testimonial 3 start -->
    <div class="testimonial testimonial-3">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="testimonial-inner">
                        <header class="testimonia-header">
                            <h1>Testimonial</h1>
                        </header>
                        <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active">
                                </li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <p class="lead"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                        in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla.
                                        Nulla posuere sapien vitae. </p>
                                    <div class="avatar">
                                        <img src="assets/img/avatar/avatar-2.jpg" alt="avatar-2" class="img-fluid rounded">
                                    </div>
                                    <ul class="rating">
                                        <li>
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-half-full"></i>
                                        </li>
                                    </ul>
                                    <div class="author-name"> John Antony </div>
                                </div>
                                <div class="carousel-item">
                                    <p class="lead"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                        in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla.
                                        Nulla posuere sapien vitae. </p>
                                    <div class="avatar">
                                        <img src="assets/img/avatar/avatar.jpg" alt="avatar" class="img-fluid rounded">
                                    </div>
                                    <ul class="rating">
                                        <li>
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-half-full"></i>
                                        </li>
                                    </ul>
                                    <div class="author-name"> Martin Smith </div>
                                </div>
                                <div class="carousel-item">
                                    <p class="lead"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                        in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla.
                                        Nulla posuere sapien vitae. </p>
                                    <div class="avatar">
                                        <img src="assets/img/avatar/avatar-3.jpg" alt="avatar-3" class="img-fluid rounded">
                                    </div>
                                    <ul class="rating">
                                        <li>
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-half-full"></i>
                                        </li>
                                    </ul>
                                    <div class="author-name"> Karen Paran </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Testimonial 3 end -->
    <!-- Blog start -->
    <!-- Blog end -->
    <!-- intro section start -->
    <!-- intro section end -->
    <!-- Footer start --> @include('shared.footer')
    <!-- Footer end -->
    <!-- Full Page Search -->
    <div id="full-page-search">
        <button type="button" class="close">×</button>
        <form action="#">
            <input type="search" value="" placeholder="type keyword(s) here" />
            <button type="button" class="btn btn-sm btn-color">Search</button>
        </form>
    </div>
    <!-- Property Video Modal -->
    <div class="modal property-modal fade" id="propertyModal" tabindex="-1" role="dialog" aria-labelledby="propertyModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="propertyModalLabel"> Find Your Dream Properties </h5>
                    <p>
                        <i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i> 123 Kathal St. Tampa
                        City, </p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6 modal-left">
                            <div class="modal-left-content">
                                <div id="modalCarousel" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner" role="listbox">
                                        <div class="carousel-item active">
                                            <iframe class="modalIframe" src="https://www.youtube.com/embed/5e0LxrLSzok" allowfullscreen></iframe>
                                        </div>
                                        <div class="carousel-item">
                                            <img src="assets/img/property-1.jpg" alt="Test ALT">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="assets/img/property-1.jpg" alt="Test ALT">
                                        </div>
                                    </div>
                                    <a class="control control-prev" href="#modalCarousel" role="button" data-slide="prev">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a class="control control-next" href="#modalCarousel" role="button" data-slide="next">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                                <div class="description">
                                    <h3>Description</h3>
                                    <p> Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque
                                        massa, viverra interdum eros ut, imperdiet pellentesque mauris. Proin sit amet
                                        scelerisque risus. Donec </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 modal-right">
                            <div class="modal-right-content bg-white">
                                <strong class="price"> $178,000 </strong>
                                <section>
                                    <h3>Features</h3>
                                    <ul class="bullets">
                                        <li><i class="flaticon-bed"></i> Double Bed</li>
                                        <li><i class="flaticon-swimmer"></i> Swimming Pool</li>
                                        <li><i class="flaticon-bath"></i> 2 Bathroom</li>
                                        <li><i class="flaticon-car-repair"></i> Garage</li>
                                        <li><i class="flaticon-parking"></i> Parking</li>
                                        <li><i class="flaticon-theatre-masks"></i> Home Theater</li>
                                        <li><i class="flaticon-old-typical-phone"></i> Telephone</li>
                                        <li><i class="flaticon-green-park-city-space"></i> Private space</li>
                                    </ul>
                                </section>
                                <section>
                                    <h3>Overview</h3>
                                    <dl>
                                        <dt>Area</dt>
                                        <dd>2500 Sq Ft:3400</dd>
                                        <dt>Condition</dt>
                                        <dd>New</dd>
                                        <dt>Year</dt>
                                        <dd>2018</dd>
                                        <dt>Price</dt>
                                        <dd>$178,000</dd>
                                    </dl>
                                </section>
                                <a href="properties-details.html" class="btn btn-show btn-theme">Show Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Off-canvas sidebar -->
    <!-- <div class="off-canvas-sidebar">
    <div class="off-canvas-sidebar-wrapper">
        <div class="off-canvas-header">
            <a class="close-offcanvas" href="#"><span class="fa fa-times"></span></a>
        </div>
        <div class="off-canvas-content">
            <aside class="canvas-widget">
                <div class="logo-sitebar text-center">
                    <img src="assets/img/logos/logo.png" alt="logo">
                </div>
            </aside>
            <aside class="canvas-widget">
                <ul class="menu">
                    <li class="menu-item menu-item-has-children"><a href="index.html">Home</a></li>
                    <li class="menu-item"><a href="properties-grid-leftside.html">Properties List</a></li>
                    <li class="menu-item"><a href="properties-details.html">Property Detail</a></li>
                    <li class="menu-item"><a href="blog-single-sidebar-right.html">Blog</a></li>
                    <li class="menu-item"><a href="about-1.html">About  US</a></li>
                    <li class="menu-item"><a href="contact-1.html">Contact US</a></li>
                </ul>
            </aside>
            <aside class="canvas-widget">
                <ul class="social-icons">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-vk"></i></a></li>
                </ul>
            </aside>
        </div>
    </div>
</div> --> @include('shared.scripts')
</body>
<!-- Mirrored from storage.googleapis.com/themevessel-products/xero/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Oct 2019 09:18:15 GMT -->

</html>