@extends('layouts.master1')
@section('content')
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>Properties Grid</h1>
            <ul class="breadcrumbs">
                <li><a href="{{ url('/')}}">Home</a></li>
                <li class="active">These are the picked {{$bathrooms}} bathrooms 15 New Listed Properties</li>
            </ul>
        </div>
    </div>
</div>
<div class="properties-list-rightside content-area-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="option-bar d-none d-xl-block d-lg-block d-md-block d-sm-block">
                    <div class="row clearfix">
                        <div class="col-xl-4 col-lg-5 col-md-5 col-sm-5">
                            <h4>
                                <span class="heading-icon">
                                    <i class="fa fa-caret-right icon-design"></i>
                                    <i class="fa fa-th-large"></i>
                                </span>
                                <span class="heading">Houses Found</span>
                            </h4>
                        </div>
                        <!-- <div class="col-xl-8 col-lg-7 col-md-7 col-sm-7">
                            <div class="sorting-options clearfix">
                                <a href="properties-list-rightside.html" class="change-view-btn"><i class="fa fa-th-list"></i></a>
                                <a href="properties-grid-rightside.html" class="change-view-btn active-view-btn"><i class="fa fa-th-large"></i></a>
                            </div>
                            <div class="search-area">
                                <select class="selectpicker search-fields" name="location">
                                    <option>High to Low</option>
                                    <option>Low to High</option>
                                </select>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="subtitle">{{$items_count}} Result Found </div>
                <div class="row">
                    @foreach ($properties as $p)
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="property-box">
                            <div class="property-thumbnail">
                                <a href="properties-details.html" class="property-img">
                                    @if($p->is_featured = 'true')
                                    <div class="tag button alt featured">featured</div>
                                    @else
                                    <div class="tag button alt featured">Not featured</div>
                                    @endif <div class="price-ratings-box">
                                        <div class="date-box">{{$p->listing_for}}</div>
                                        <p class="price"> $178,000 </p>
                                        <div class="ratings">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                    </div>
                                    <img src="assets/img/property-2.jpg" alt="property-2" class="img-fluid">
                                </a>
                                <div class="property-overlay">
                                    <a href="properties-details.html" class="overlay-link">
                                        <i class="fa fa-link"></i>
                                    </a>
                                    <a class="overlay-link property-video" title="Test Title">
                                        <i class="fa fa-video-camera"></i>
                                    </a>
                                    <div class="property-magnify-gallery">
                                        <a href="assets/img/property-2.jpg" class="overlay-link">
                                            <i class="fa fa-expand"></i>
                                        </a>
                                        <a href="assets/img/property-6.jpg" class="hidden"></a>
                                        <a href="assets/img/property-4.jpg" class="hidden"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="detail">
                                <h1 class="title">
                                    <a href="properties-details.html">{{$p->title}}</a>
                                </h1>
                                <div class="location">
                                    <a href="properties-details.html">
                                        <i class="fa fa-map-marker"></i>{{$p->address}} </a>
                                </div>
                                <ul class="facilities-list clearfix">
                                    <li>
                                        <i class="flaticon-bed"></i>{{$p->room}} Bedrooms </li>
                                    <li>
                                        <i class="flaticon-bath"></i> {{$p->bathroom}} Bathrooms </li>
                                    <li>
                                        <i class="flaticon-square-layouting-with-black-square-in-east-area"></i> Sq
                                        Ft:{{$p->area}} </li>
                                    <li>
                                        <i class="flaticon-car-repair"></i> {{$p->garage}} Garage </li>
                                </ul>
                            </div>
                            <div class="footer">
                                <a href="#">
                                    <i class="fa fa-user"></i> {{$p->author}} </a>
                                <span>
                                    <i class="fa fa-calendar-o"></i> {{$p->Date}} </span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-lg-12">
                        <div class="pagination-box hidden-mb-45 text-center">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item"><a class="page-link" href="#"><span aria-hidden="true">«</span></a></li>
                                    <li class="page-item"><a class="page-link active" href="properties-grid-rightside.html">1</a></li>
                                    <li class="page-item"><a class="page-link" href="properties-grid-leftside.html">2</a></li>
                                    <li class="page-item"><a class="page-link " href="properties-grid-fullwidth.html">3</a></li>
                                    <li class="page-item"><a class="page-link" href="properties-grid-leftside.html"><span aria-hidden="true">»</span></a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="sidebar mbl">
                    <!-- Search area start -->
                    <div class="widget search-area">
                        <h5 class="sidebar-title">Advanced Search</h5>
                        <div class="search-area-inner">
                            <div class="search-contents ">
                                <form action="{{ action('HouseController@search') }}" method="post" id="postData">
                                    @csrf
                                    <div class="form-group">
                                        <label>Area From</label>
                                        <select class="selectpicker search-fields" name="area">
                                            <option>Area From</option>
                                            <option>1500</option>
                                            <option>1200</option>
                                            <option>900</option>
                                            <option>600</option>
                                            <option>300</option>
                                            <option>100</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Property Status</label>
                                        <select class="selectpicker search-fields" name="status">
                                            <option>Property Status</option>
                                            <option value="Sale">For Sale</option>
                                            <option value="Rent">For Rent</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Location</label>
                                        <select class="selectpicker search-fields" name="location">
                                            <option>Location</option>
                                            <option>United Kingdom</option>
                                            <option>American Samoa</option>
                                            <option>Belgium</option>
                                            <option>Canada</option>
                                            <option>Delaware</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Property Types</label>
                                        <select class="selectpicker search-fields" name="p_types">
                                            <option value="">Property Types</option>
                                            <option value="Sale">Sale</option>
                                            <option value="Rent">Rent</option>
                                            <option>Land</option>
                                            <!-- @foreach ($properties as $p)
                                            <option value="{{ $p -> listing_for}}">{{ $p -> listing_for}}</option>
                                            @endforeach -->
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Bedrooms</label>
                                        <select class="selectpicker search-fields" name="bedrooms">
                                            <option>Bedrooms</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Bathrooms</label>
                                        <select class="selectpicker search-fields" name="bathrooms">
                                            <option>Bathrooms</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label>Price</label>
                                        <div class="range-slider">
                                            <div data-min="0" data-max="150000" data-unit="USD" data-min-name="min_price" data-max-name="max_price" class="range-slider-ui ui-slider" aria-disabled="false"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <br>
                                    <h5 class="sidebar-title">Features</h5>
                                    <div class="form-group">
                                        <div class="form-check checkbox-theme">
                                            <input class="form-check-input" type="checkbox" value="" id="audi">
                                            <label class="form-check-label" for="audi"> Air Condition </label>
                                        </div>
                                        <div class="form-check checkbox-theme">
                                            <input class="form-check-input" type="checkbox" value="" id="honda">
                                            <label class="form-check-label" for="honda"> Free Parking </label>
                                        </div>
                                        <div class="form-check checkbox-theme">
                                            <input class="form-check-input" type="checkbox" value="" id="volkswagen">
                                            <label class="form-check-label" for="volkswagen"> Swimming Pool </label>
                                        </div>
                                        <div class="form-check checkbox-theme">
                                            <input class="form-check-input" type="checkbox" value="" id="lamborghini">
                                            <label class="form-check-label" for="lamborghini"> Laundry Room </label>
                                        </div>
                                        <div class="form-check checkbox-theme">
                                            <input class="form-check-input" type="checkbox" value="" id="bmw">
                                            <label class="form-check-label" for="bmw"> Central Heating </label>
                                        </div>
                                        <div class="form-check checkbox-theme">
                                            <input class="form-check-input" type="checkbox" value="" id="toyota">
                                            <label class="form-check-label" for="toyota"> Window Covering </label>
                                        </div>
                                    </div>
                                    <br />
                                    <button type="submit" class=" btn-md btn-color">Search</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Categories start -->
                    <div class="widget categories">
                        <h5 class="sidebar-title">Categories</h5>
                        <ul>
                            <li><a href="#">Apartments<span>(12)</span></a></li>
                            <li><a href="#">Houses<span>(8)</span></a></li>
                            <li><a href="#">Family Houses<span>(23)</span></a></li>
                            <li><a href="#">Offices<span>(5)</span></a></li>
                            <li><a href="#">Villas<span>(63)</span></a></li>
                            <li><a href="#">Other<span>(7)</span></a></li>
                        </ul>
                    </div>
                    <!-- Recent posts start -->
                    <div class="widget recent-posts">
                        <h5 class="sidebar-title">Recent Properties</h5>
                        <div class="media mb-4">
                            <a href="properties-details.html">
                                <img src="assets/img/sub-property/sub-property.jpg" alt="sub-property">
                            </a>
                            <div class="media-body align-self-center">
                                <h5>
                                    <a href="properties-details.html">Beautiful Single Home</a>
                                </h5>
                                <p>February 27, 2018</p>
                                <p> <strong>$245,000</strong></p>
                            </div>
                        </div>
                        <div class="media mb-4">
                            <a href="properties-details.html">
                                <img src="assets/img/sub-property/sub-property-2.jpg" alt="sub-property-2">
                            </a>
                            <div class="media-body align-self-center">
                                <h5>
                                    <a href="properties-details.html">Sweet Family Home</a>
                                </h5>
                                <p>February 27, 2018</p>
                                <p> <strong>$245,000</strong></p>
                            </div>
                        </div>
                        <div class="media">
                            <a href="properties-details.html">
                                <img src="assets/img/sub-property/sub-property-3.jpg" alt="sub-property-3">
                            </a>
                            <div class="media-body align-self-center">
                                <h5>
                                    <a href="properties-details.html">Real Luxury Villa</a>
                                </h5>
                                <p>February 27, 2018</p>
                                <p> <strong>$245,000</strong></p>
                            </div>
                        </div>
                    </div>
                    <!-- Recent comments start -->
                    <div class="recent-comments widget">
                        <h5 class="sidebar-title">Recent comments</h5>
                        <div class="media mb-4">
                            <a class="pr-4" href="#">
                                <img src="assets/img/avatar/avatar.jpg" class="rounded-circle" alt="avatar">
                            </a>
                            <div class="media-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <p>By <span>John Doe</span></p>
                            </div>
                        </div>
                        <div class="media">
                            <a class="pr-4" href="#">
                                <img src="assets/img/avatar/avatar-2.jpg" class="rounded-circle" alt="avatar-2">
                            </a>
                            <div class="media-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <p>By <span>Karen Paran</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> @stop